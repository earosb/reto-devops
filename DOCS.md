### Reto 2. Docker Compose

> 2. Asegurar el endpoint /private con auth_basic

Para crear un usuario y pass ejecutar

```shell
htpasswd -c docker/nginx/.htpasswd user
```

Credenciales por defecto

    user default
    pass secret123

> 3. Habilitar https y redireccionar todo el trafico 80 --> 443

La generación del certificado se hizo de manera manual para el dominio localhost con el siguiente comando:

```
openssl req -x509 -out localhost.crt -keyout localhost.key \
  -newkey rsa:2048 -nodes -sha256 \
  -subj '/CN=localhost' -extensions EXT -config <( \
   printf "[dn]\nCN=localhost\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:localhost\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")
```

> https://letsencrypt.org/docs/certificates-for-localhost/#making-and-trusting-your-own-certificates

La razón por al que generamos el certificado local es porque el propósito de la configuración docker-compose es para desarrollo. En un ambiente prod utilizariamos una solución como certbot para generar este certificado.

### Reto 3. Probar la aplicación en cualquier sistema CI/CD

Configurado en gitlabci stages test y build.

- test_app Ejecuta test de app
- build_image Construye y sube imagen a registry

### Reto 4. Deploy en kubernetes

Los manifiestos del cluster están en el directorio k8s, se expone la app mediante un servicio de tipo NodePort para efectos del ejercicio. En un ambiente prod utilizariamos un ingress

> añade Horizontal Pod Autoscaler a la app NodeJS

Requiere instalar [Metrics server](https://github.com/kubernetes-sigs/metrics-server) en el cluster utilizado.

Se configuraron metricas bajas en el deployment para forzar el ejercicio de escalamiento

```
resources:
    limits:
      cpu: 200m
    requests:
      cpu: 20m
```

Para probar el hpa se utilizó apache benchmark

```
# 100 request concurrentes de un total de 10000
ab -c 100 -n 10000 $NODE_IP:$SVC_PORT
```