ARG NODE_VERSION='16.13'

FROM node:${NODE_VERSION}-alpine

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

# Create a group and user with default UID=1000
RUN addgroup -S www && \
    adduser -S www -G www

# Tell docker that all future commands should run as the www user
USER www

EXPOSE 3000

CMD [ "node", "index.js" ]
